# odp2md reloaded
This Perl script converts a Open/Libre Office Impress document (*.odp) to markdown. 

## Based on: 
- https://sourceforge.net/p/odp2md/code/ci/master/tree/ (see also: http://www.gillesmaire.com/tiki-index.php?page=odp2md)

## License:
- unclear / no license given (will send email to maintainer of odp2md to find out!)

## Dependencies: 
- ODF::lpOD (documentation: http://search.cpan.org/~jmgdoc/ODF-lpOD-1.126/)
    - ODF::lpOD itself has a few dependencies, as listed here: http://cpansearch.perl.org/src/JMGDOC/ODF-lpOD-1.126/INSTALL

## Usage:
```odp2md file.odp```

This will create ```file.md``` in the current working directory. If there are any pictures embedded in the slides, it will extract these pictures and put them into the folder 'Pictures' in the current working directory. 

# Bugs: 
See the [issue tracker](https://gitlab.tubit.tu-berlin.de/ziik/odp2md/issues)